let socket = io();
let canvas = document.getElementById("canvas")
let ctx = canvas.getContext("2d")
let images = []
let index = 0

let time = 0
let speed = 1 / 10
let last_time = 0

let running = false
let frame_time = 0
let fps = 0.5
let frame_delta = 0
let frame_target = 1 / 60


let frames = []

let render_times = []
let countdown = 0

let fullscreen = false

window.addEventListener("keydown", (e) =>{
  console.log(e)
  if(e.key != "f")
    return
  let element = document.querySelector("canvas");

  if(!fullscreen)
    element.requestFullscreen()
      .then(function() {
        fullscreen = true
      // element has entered fullscreen mode successfully
      })
      .catch(function(error) {
        // element could not enter fullscreen mode
      });
  else{
    document.exitFullscreen()
    fullscreen = false
  }

})

let options = {
  fps : 0.5,
  fade : 1.0,
  frame:0
}

let params = null

function average(a){
  return a.length == 0 ? 0 : a.reduce(((a, v, i) => a + v), 0) / a.length
}

function smoothstep (min, max, value) {
  var x = Math.max(0, Math.min(1, (value-min)/(max-min)));
  return x*x*(3 - 2*x);
};
let clamp = (v, a, b) => Math.max(Math.min(v, b), a)
function draw_fit_image() {
  let w = innerWidth
  let h = innerHeight
  let s = Math.min(w, (fullscreen ? h : h - 100))
  let x = ! fullscreen ? 0 : (w > h ? (w - h) / 2 : 0)
  let y = ! fullscreen ? 0 : (h > w ? (h - w) / 2 : 0)
  if(images.length > 0 && images[options.frame]){
    let index2 = (options.frame + 1) % images.length
    ctx.globalAlpha = 1.0
    ctx.drawImage(images[options.frame], x, y,s,s);
    ctx.globalAlpha = clamp(frame_time + (1 - options.fade), 0, 1)
    ctx.drawImage(images[index2], x,y,s,s);
    ctx.globalAlpha = 1.0
  }else{
    ctx.font = '16px monospace';
    ctx.fillStyle = "white"
    ctx.fillText(`waiting for first frame...`, 10, 50); 
  }
}
let zp = (i) => i < 10 ? "0" + i : "" + i 
function print_time(s) {
  s = Math.floor(s)
  let m = Math.floor(s / 60)
  s = s % 60
  let h = Math.floor(m / 60)
  m = m % 60
  return `${zp(h)}:${zp(m)}:${zp(s)}` 
}
function update(t){
  window.requestAnimationFrame(update)
  delta = (t - last_time) * 0.001
  countdown = Math.max(0, countdown - delta)
  frame_delta += delta
  if(frame_delta > frame_target){
    ctx.clearRect(0,0,innerWidth,innerHeight)
    frame_delta -= frame_target
  }else{
    return null
  }
  last_time = t
  time += speed * delta
  time = time - Math.floor(time)

  frame_time += options.fps * delta
  if(frame_time > 1.0 && images.length > 0){
    frame_time = frame_time - Math.floor(frame_time)
    options.frame = (options.frame + 1) % images.length
  }
  frame_time = frame_time - Math.floor(frame_time)


  if(params != null){
    if(images.length != params.segmaps.length){
      draw_loading_bar(time)
      // ctx.font = '12px monospace';
      // ctx.fillStyle = "white"
      // ctx.fillText(print_time(countdown), innerWidth - 100, innerHeight - 10);      
    }
    // status()
  }
  draw_fit_image()
}
window.requestAnimationFrame(update)
function status(){
  ctx.font = '32px monospace';
  ctx.fillStyle = "white"
  ctx.fillText(`${images.length}/${params.segmaps.length}`, 10, innerHeight - 50); 
  ctx.fillText(`${options.frame}`, innerWidth - 100, innerHeight - 50); 
}
function draw_loading_bar(t){

  let w = innerWidth
  let h = innerHeight
  let b = 2

  ctx.fillStyle = "black"
  ctx.fillRect(0,h - b, w, b)
  ctx.fillStyle = "white"
  ctx.fillRect(0,h - b, w * t, b)
}

function draw_frame_bar(ym){

  let w = innerWidth
  let h = innerHeight
  let b = 40

  let c = params.segmaps.length 

  let sw = w / c


  for(let x = 0; x<params.segmaps.length; x++){
    if(x < images.length){
      ctx.drawImage(images[x], x * sw,h,b,b);
    }else if(x == images.length){
      let p = {
        x : sw * 0.5,
        y : sw * 0.5,
      }

      ctx.strokeStyle = "white"
      let half = b * 0.5
      ctx.save()
      ctx.translate(sw * x - half * 0.5, h - half * 0.5)
      ctx.beginPath();
      let t = time < 0.5 ? time * 2 : 1.0
      ctx.moveTo(p.x - t * half, p.y + half);
      ctx.lineTo(p.x + t * half, p.y + half);

      ctx.moveTo(p.x - t * half, p.y - half);
      ctx.lineTo(p.x + t * half, p.y - half);

      if(time > 0.5){
        let t = 1 - ((time - 0.5) * 2)
        ctx.moveTo(p.x - half, p.y - half);
        ctx.lineTo(p.x - half, p.y - half * t);
        ctx.moveTo(p.x + half, p.y - half);
        ctx.lineTo(p.x + half, p.y - half * t);


        ctx.moveTo(p.x - half, p.y + half);
        ctx.lineTo(p.x - half, p.y + half * t);
        ctx.moveTo(p.x + half, p.y + half);
        ctx.lineTo(p.x + half, p.y + half * t);

      }

      ctx.stroke();
      ctx.restore()
    }else{
      ctx.fillStyle = "#999999"
      ctx.fillRect(0 + sw * x, h + b, b, 2)
    }
  }
}
function frame(x) {

  segmaps = x.segmaps
  console.log("frame")
  console.log(x)
  if(images.length == x.segmaps.length){
    // modify existing image
    let img = images.findIndex((i) => i.name == x.name)

    console.log("modifying frame", images[img].name)
    images[img].onload = function() {

    }
    images[img].src = x.name + "?t=" + new Date().getTime();
  }else{
    render_times.push(x.duration)
    countdown = average(render_times) * (x.segmaps.length - images.length)
    speed = 1 / x.duration
    time = 0
    let img = new Image();
    img.name = x.name
    let file = x.name
    img.onload = function() {
      images.push(img)
      console.log("pushing into images", images)
    }
    img.src = file
  }
}

let image_buffer = []
let loaded = 0

function load_images(ls){
  image_buffer = []
  for(let i = 0; i < ls.length; i++){
    let img = new Image();
    let file = ls[i]
    img.name = file
    img.onload = function(){
      loaded++
      if(loaded == ls.length){
        running = true
        images = image_buffer.map((i) => i)
        image_buffer = []
      }
    }
    image_buffer.push(img)
    img.src = file
  }

}
function create_image(x){
  let i = new Image()

  return i;
}
function begin(o) {
  console.log("begin")
  console.log(o)
  params = o.params
  images = []
  frames = o.segmaps.map(create_image)
  options.frame = 0
  time = 0
  frame_time = 0
  speed = 0.1

  if(o.images.length > 0){
    load_images(o.images)
  }else{
    running = true
  }
  init_gui()
}
socket.on("frame", frame)
socket.on("begin", begin)
console.log(socket)
function resize() {
  let w = innerWidth
  let h = innerHeight

  let s = Math.min(w, h)
  canvas.width = w
  canvas.height = h
  // draw_fit_image()
}
window.addEventListener("resize", resize)
resize()

let last_fps = 0
function init_gui(){
  const gui = new dat.GUI()
  // const gui_folder = gui.addFolder('gauganeer')
  gui.add(options, 'frame', 0, params.segmaps.length - 1)
    .step(1)
    .listen()
    .onChange((e)=> {
      frame_time = 0
      // last_fps = options.fps + 0
      // options.fps = 0
    })
    .onFinishChange((e)=>{
      // options.fps = last_fps
    })
  gui.add(options, 'fps', 0, 60).listen
  gui.add(options, 'fade', 0, 1)
}
// gui_folder.open()