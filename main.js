const fs = require("fs")
const ba64 = require("ba64")
const fetch = require("node-fetch")

const puppeteer = require('puppeteer')

const pipeline = require('stream').pipeline
const promisify = require('util').promisify

// for converting color palettes
const jimp = require("jimp")

// for preview server
const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
const ip = require("ip")

let get_style = function() {
  let i = process.argv[3]
  if(!isNaN(i)){
    return i % 11
  }else{
    return Math.round(Math.random() * 10)
  }
}
let any_arg_is = function(t){
  return (process.argv[3] == t || process.argv[4] == t)
}

let params = {
  folder : process.argv[2],
  segmaps : [],
  default_style : get_style()
}
let config = {
  resume : any_arg_is("resume"),
  headless : !any_arg_is("window"),
  // debug : false,
  debug : any_arg_is("debug"),
  colormap : "custom_palette.png",
  port : 8000
}

let browser = null
let page = null

let variables = null

let segmaps = []
let styles = []
let sketches = []
let texts = []
let text_files = []
let processing = false
let images = []
let color_map = []

let buffer = []

let last_style = ""

function repeat_char(char, c){
  r = ""
  for(let i = 0; i<c; i++)
    r = r + char
  return r
}
function preview_server(port){
  app.use(express.static('.'))

  let conn = (socket) => {
    socket.emit("begin", {params : params, images : (segmaps.map((x) => params.folder + "/output/" + x)), segmaps : segmaps})
  }
  io.on('connection', conn);
  io.on('reconnect', conn);

  server.listen(port, () => {
    let addr = "http://"+ ip.address() + ':' + port
    console.log("\n==== preview running")
    console.log(repeat_char("=", addr.length))
    greenlog(addr)
    console.log(repeat_char("=", addr.length) + "\n")
  })
}

async function load_colormap(path) {
  cyanlog("-- loading palette : " + path)
  let map = []
  await jimp.read("default_palette.png").then((def) =>{
    jimp.read(path).then((custom) =>{
      for(let y = 0; y < 4; y++){
        for(let x = 0; x < 11; x++){
          p = def.getPixelIndex(x, y)
          let a = def.bitmap.data[p + 3]
          if(a != 0){
            map.push({
              a : [
                def.bitmap.data[p + 0],
                def.bitmap.data[p + 1],
                def.bitmap.data[p + 2]
              ],
              b : [
                custom.bitmap.data[p + 0],
                custom.bitmap.data[p + 1],
                custom.bitmap.data[p + 2]
              ]
            })
          }
        }
      }
    })
  })
  return map
}

async function _recolor_image(input, output, colormap) {
  // return new Promise((resolve, reject) =>{
  let recolor = async function(img) {
    // console.log(img)
    let c = [0,0,0]
    await img.scan(0,0,img.bitmap.width, img.bitmap.height, function(x, y, p){
      c[0] = this.bitmap.data[p + 0]
      c[1] = this.bitmap.data[p + 1]
      c[2] = this.bitmap.data[p + 2]
      for(let i = 0; i<colormap.length; i++){
        let b = colormap[i].b
        if(b[0] == c[0] && b[1] == c[1] && b[2] == c[2]){
          this.bitmap.data[p + 0] = colormap[i].a[0]
          this.bitmap.data[p + 1] = colormap[i].a[1]
          this.bitmap.data[p + 2] = colormap[i].a[2]
          break;
        }
      }
    })
    await img.writeAsync(output, (e)=>console.log("ff", e))
  }
  let im = await jimp.read(input)
  await recolor(im)
  greenlog(">> " + input)
  // console.log("fffs", im)
  // })
}

async function recolor_image(n) {
  await _recolor_image(params.folder + "/" + n, input_file(n), color_map)
}

let debug = (t) => {
  if(config.debug)
    console.log(t)
}


let colog = (t, c) => console.log("\x1b["+c+"m"+t+"\x1b[0m")  
let redlog = (t) => colog(t, "31")
let cyanlog = (t) => colog(t, "36")
let greenlog = (t) => colog(t, "32")

function ensure_folder(root, dir){
  if (!fs.existsSync(root + "/" + dir)) fs.mkdirSync(root + "/" + dir, { recursive: true });
}
function gather_pngs(folder){
  if(fs.existsSync(folder))
    return fs.readdirSync(folder).filter((n)=>n.split(".")[1] == "png")
  else
    return []
}
function filter_no_pairs(segmaps, paired){
  return {
    filtered : paired.filter((x) => {
      let pair = segmaps.find((s) => s.split(".")[0] == x.split(".")[0])
      return pair != null
    }),
    orphans : paired.filter((x) => {
      let pair = segmaps.find((s) => s.split(".")[0] == x.split(".")[0])
      return pair == null
    })
  }
}
function clear_orphans(segmaps, paired, type){
  let ps = filter_no_pairs(segmaps, paired)
  if(ps.orphans.length > 0){
    redlog("-- orphan files in /" + type + " : " + ps.orphans)
  }
  return ps.filtered
}
function dependent_segmaps (type, name) {
  switch(type){
    case "segmap" : return [name]; break;
    case "sketch" : return [name]; break;
    case "text" : 
      return [name.split(".")[0] + "png"]; 
    break;
    case "style" :{
      last_style = ""
      let r = [name]
      for(let i = segmaps.indexOf(name); i<segmaps.length; i++){
        if(i != segmaps.indexOf(name))
          if(styles.find((x) => x == segmaps[i]) == null)
            r.push(segmaps[i])
          else
            break;
      }
      return r
    }break;
  }
  return []
}
function file_change(type, name) {
  cyanlog("~~ " + type + " changed : " + name + "\n")
  let dep = dependent_segmaps(type, name)
  debug("dependecies")
  debug(dep)
  if(dep.length > 1)
    console.log("~~ dependent frames : ", dep, "\n")
  buffer = buffer.filter((x) => !dep.includes(x))
  buffer = buffer.concat(dep)
  if(!processing)
    process_buffer()
}

async function process_buffer(){
  processing = true
  let target = buffer.shift()
  console.log("   " + target)
  cyanlog("-- converting palette")

  let c = await recolor_image(target)

  let out = await get_image(variables, render_params(target))
  if(isNaN(out) && out < 0){
    redlog("nvidia said no. restarting...")
    buffer.unshift(target)
  }else{
    // images.push(out.output)
    io.emit("frame", {name : out.output, duration : out.duration, segmaps : segmaps})

  }

  if(buffer.length > 0)
    process_buffer()
  else{
    processing = false
    greenlog("==== done, waiting for file changes...\n")
  }
}

function gather_inputs() {
  cyanlog("-- gathering inputs")
  console.log("/" + params.folder)

  segmaps = gather_pngs(params.folder)
  if(segmaps.length > 0){
    console.log(segmaps)
  }else{
    redlog("!! no segmaps found in /" + params.folder)
  }

  styles = gather_pngs(params.folder + "/style")
  sketches = gather_pngs(params.folder + "/sketch")

  styles = clear_orphans(segmaps, styles, "style")
  sketches = clear_orphans(segmaps, sketches, "sketch")

  let text_files = fs.existsSync(params.folder + "/text")
    ? fs.readdirSync(params.folder + "/text").filter((n)=>n.split(".")[1] == "txt")
    : []

  text_files = clear_orphans(segmaps, text_files, "text")

  texts = text_files.map((t) => {
    return {
      name : t.split(".")[0] + ".png",
      text : fs.readFileSync(params.folder + "/text/" + t, "utf8").replace(/(\r\n|\n|\r)/gm, "")
    }
  })
  if(styles.length > 0){
    console.log("--/style")
    console.log(styles)
  }else{
    console.log("-- no styles, using style ["+ params.default_style + "]")
  }
  if(sketches.length > 0){
    console.log("--/sketch")
    console.log(sketches)
  }else{
    console.log("-- no sketches")
  }
  if(texts.length > 0){
    console.log("--/text")
    console.log(text_files)
  }else{
    console.log("-- no texts")
  }

  params.segmaps = segmaps
  console.log()

  watch_files("segmap", segmaps)
  watch_files("style", styles)
  watch_files("sketch", sketches)
  watch_files("text", texts)
}

function watch_files(type, ls){
  debug("watching files " + type)
  for(let x of ls){
    let name = params.folder + "/"
    if(type == "segmap")
      name = name + x
    else
      name = name + type + "/" + x

    fs.watchFile(name, {persistent:true, interval:250}, (curr, prev) => file_change(type, x)
    )
  }
}

async function init_puppet(){
  browser = await puppeteer.launch({
    // waitUntil: 'networkidle0',
    headless: config.headless,
    defaultViewport: null,
    devtools : true,
  })
  const pages = await browser.pages()
  page = pages[0]

  await page.exposeFunction("export_variables", function(v){
    variables = v
    start(variables)
  })

  cyanlog("-- loading http://gaugan.org/gaugan2")
  await page.goto("http://gaugan.org/gaugan2")
  await page.evaluate(() => export_variables({name : global_fn, urls : urls}))

}

async function init(){
  console.log("let's jam...")
  gather_inputs()
  ensure_folder(params.folder, "input")
  ensure_folder(params.folder, "output")

  color_map = await load_colormap(config.colormap)


  await init_puppet()
}

function now() {
  return performance.now()
}
function fixed(x){
  return Number.parseFloat(x).toFixed(2)
}

Clock = {
  start_time : 0,
  segment_time : 0,
  start : function(){
    this.start_time = now()
  },
  start_segment : function(){
    this.segment_time = now()
  },
  get_segment : function(){
    return fixed((now() - this.segment_time) / 1000)
  },
  get_complete : function(){
    return fixed((now() - this.start_time) / 1000)
  }
}
async function start(variables) {

  if(config.resume){
    console.log("\n== resume mode, skipping render")
    greenlog("== waiting for file changes...\n")
  }else{
    console.log("\n==== rendering...\n")
    buffer = segmaps.map((x) => x + "")
    process_buffer()
  }
  preview_server(config.port)
}
function input_file(f){
  return params.folder + "/input/" + f
}
function find_pair(type, arr, target){
  if(arr.find((i) => i == target))
    return params.folder + "/" + type + "/" + target
  else
    return false
}

function find_style_backwards(target){
  if(styles.length > 0){
    let index = segmaps.indexOf(target)
    for(let i = index; i >= 0; i--){
      if(styles.includes(segmaps[i]))
        return params.folder + "/style/" + segmaps[i]
    }
    return false
  }else{
    return false
  }
}
function render_params(input) {
  let segmap = input_file(input)
  let sketch = find_pair("sketch", sketches, input)
  let text = texts.find((t) => t.name == input)
  let style = find_style_backwards(input)

  return {
    output : params.folder + "/output/" + input,
    // segmap : params.folder +"/"+ input,
    segmap : input_file(input),
    sketch : sketch,
    style : style,
    text : text == null ? "" : text.text
  }
}

function b64(file) {
  return "data:image/png;base64," + fs.readFileSync(file, "base64");
}

async function set_style(variables, style){
  if(last_style != style){
    cyanlog("-- uploading style " + style)
    let style_image = b64(style)
    let url = variables.urls[0]
    const d = new URLSearchParams();
    d.append('name', variables.name);
    d.append("file", style_image)

    const response = await fetch(url + "gaugan2_receive_style_image", {method: 'POST', body: d});
    try{
      const data = await response.json();
    }catch(e){
      return -1
    }
    last_style = style
  }else{
    debug("already using style")
  }
  return 0
}
async function get_image(variables, input){
  debug(input)
  Clock.start_segment()
  let output = input.output
  let segmap_image = b64(input.segmap)
  let sketch_image = input.sketch ? b64(input.sketch) : ""

  if(input.style){
    let s = await set_style(variables, input.style)
    if(s == -1)
      return -1
  }
  let s = input.style ? "custom" : params.default_style
  // console.log(s)
  let url = variables.urls[0]
  const d = new URLSearchParams();
  d.append('name', variables.name);
  d.append("masked_segmap", segmap_image)
  d.append("masked_edgemap", sketch_image)
  d.append("masked_image", "")
  d.append("style_name", s)
  d.append("caption", input.text)
  d.append("enable_edge", input.sketch ? true : false)
  d.append("enable_seg", true)
  d.append("enable_caption", input.text != "")
  d.append("enable_image", false)
  d.append("use_model2", false)

  cyanlog("-- uploading inputs ")
  const response = await fetch(url + "gaugan2_infer", {method: 'POST', body: d});

  try{
    const data = await response.json();
  }catch(e){
    return -2
  }
  // console.log(data);

  const p = new URLSearchParams();
  p.append('name', variables.name);
  const streamPipeline = promisify(pipeline);
  const image_response = await fetch(url + "gaugan2_receive_output", {method: 'POST', body: p})


  if (!image_response.ok) return -3

  cyanlog("-- downloading image ")
  await streamPipeline(image_response.body, fs.createWriteStream(input.output));

  let duration = parseFloat(Clock.get_segment())
  greenlog(`-> ${input.output} in ${duration}s\n`)
  return {
    output : input.output,
    duration : duration
  }
}

init()
module.exports = init